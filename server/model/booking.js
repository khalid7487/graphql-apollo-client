const mongonse = require("mongoose")

const Schema = mongonse.Schema;


const bookingSchema = new Schema({
    event: {
        type: Schema.Types.ObjectId,
        ref: "Event"
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User"
    }
},
    { timestamps: true }
);

module.exports = mongonse.model('Booking', bookingSchema);