import React, { useState } from 'react'
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom'
import './App.css';
import Auth from './pages/Auth';
import Booking from './pages/Booking';
import Events from './pages/Event';

import MainNavigation from './component/Navigation/MainNavigation';
import AuthContext from './context/auth-context';


function App() {

  const [token, setToken] = useState(null)
  const [userId, setUserId] = useState(null)

  const login = (token, userId, tokenExpiration) => {
    setToken({ token: token })
    setUserId({ userId: userId })
  }

  const logout = () => {
    setToken(null);
    setUserId(null);
  }

  return (
    <Router>
      <React.Fragment>
        <AuthContext.Provider
          value={{
            token: token,
            userId: userId,
            login: login,
            logout: logout
          }}
        >
          <MainNavigation />
          <main className='main-content'>
            <Routes>
              <Route path="/" element={<Navigate replace to="/auth" />} />
              <Route path="/auth" element={<Auth />} />
              <Route path="/events" element={<Events />} />
              <Route path="/bookings" element={<Booking />} />
            </Routes>
          </main>
        </AuthContext.Provider>
      </React.Fragment>
    </Router>
  );
}

export default App;
