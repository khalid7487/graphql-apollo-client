import React from 'react';

import "./EventItem.css";

const EventListItem = (props) => {
console.log("click evetnt id", props.eventId)
    return (
        <li key={props.index} className="events__list-item">
            <div>
                <h1>{props.title}</h1>
                <h2>{props.price}</h2>
                <h2>{new Date(props.date).toLocaleDateString()}</h2>
            </div>
            <div>
                {
                    props.userId.userId === props.creatorId ? <p>Your the owner of this event.</p> :
                        <button className='btn' onClick={props.onDetails.bind(this, props.eventId)}>View Details</button>

                }

            </div>
        </li>
    )
}

export default EventListItem;