import React from 'react';

import EventItem from './EventListItem/EventListItem';

import "./EventList.css";

const EventList = props => {

    const events = props.events.map((event, index) => {
        return (
            <EventItem
                userId={props.authUserId}
                key={index} eventId={event._id}
                title={event.title}
                creatorId={event.creator._id}
                price={event.price}
                date={event.date}
                onDetails={props.onViewDetail }
            />
        )
    })
    return (
        <ul className='event__list'>
            {events}
        </ul>
    )
};

export default EventList;