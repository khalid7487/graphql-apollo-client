import React from 'react';

import './BookingList.css';

const bookingList = props => (
    <ul className='bookings__list'>
        {
            props.bookings.map((booking, index) => {
                return (

                    <li key={index} className='booking__item'>
                        <div className='bookign__item-data'>
                            {new Date(booking.createdAt).toLocaleDateString()}
                        </div>
                        <div className='booking__item-actions'>
                            <button className='btn' onClick={props.onDelete.bind(this, booking._id)}>Cancel</button>
                        </div>
                    </li>


                )
            })
        }
    </ul>
)

export default bookingList