import React, { useContext, useEffect, useState } from 'react';
import AuthContext from '../context/auth-context';

import './Event.css';

import BookingList from "../component/Booking/BookingList/BookingList"

const Booking = () => {
    const [bookings, setBookings] = useState([]);
    const context = useContext(AuthContext);

    useEffect(() => {
        fetchBookings()
    }, []);

    const fetchBookings = () => {

        const requestBody = {
            query: `
                query {
                    bookings {
                        _id
                        createdAt
                        updatedAt
                    
                  }
                }
                `
        }

        const token = context.token;

        console.log(token.token)

        fetch("http://localhost:7000/graphql", {
            method: 'POST',
            body: JSON.stringify(requestBody),
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token.token}`

            }
        })
            .then(res => {
                if (res.status !== 200 && res.status !== 201) {
                    throw new Error("Failed.")
                }
                return res.json()

            }).then(resData => {
                const bookings1 = resData.data.bookings;
                setBookings(bookings1)
            })
            .catch(err => {
                console.log(err)
            })
    }


    const deleteBookingHandler = bookingId => {

        const requestBody = {
            query: `
                mutation {
                    cancelBooking(bookingId: "${bookingId}") {
                        _id
                        title
                  }
                }
                `
        }

        const token = context.token;

        console.log(token.token)

        fetch("http://localhost:7000/graphql", {
            method: 'POST',
            body: JSON.stringify(requestBody),
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token.token}`

            }
        })
            .then(res => {
                if (res.status !== 200 && res.status !== 201) {
                    throw new Error("Failed.")
                }
                return res.json()

            }).then(resData => {
                const bookings1 = bookings.filter(booking => booking._id !== bookingId)
                console.log("after update ", bookings1)
                setBookings(bookings1)
            })
            .catch(err => {
                console.log(err)
            })


    }


    return (
        <BookingList bookings={bookings} onDelete={deleteBookingHandler} />
    )
}

export default Booking