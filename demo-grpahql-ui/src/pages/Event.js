import React, { useContext, useEffect, useState } from 'react';
import Modal from '../component/Modal/Modal';
import Backdrop from '../component/Backdrop/Backdrop';

import './Event.css';

import EventList from '../component/Events/EventList/EventList';
import AuthContext from '../context/auth-context';

const Events = () => {

  const context = useContext(AuthContext);
  const [creating, setCreating] = useState(false);
  const [events, setEvents] = useState([]);
  const [selectedEvent, setSelectedEvent] = useState(null);


  const [formData, setFormData] = useState({ title: '', price: '', date: '', description: '' });

  useEffect(() => {
    fetchEvents();
  }, [])



  const onInputChange = (e) => {
    console.log(e.target.value)
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })
  }

  const createEventHandler = () => {
    setCreating(true)
  }

  const modalConfirmHandler = () => {

    console.log("clicked ")
    if (formData.title.trim().length === 0 || formData.price <= 0 || formData.date.trim().length === 0 || formData.description.trim().length === 0) {

      return;
    }

    setCreating(false);

    const requestBody = {
      query: `
          mutation{
            createEvent(eventInput:{
              title: "${formData.title}",
              price: ${formData.price}
              description: "${formData.description}"
              date: "${formData.date}"
            }){
               title
               price
               description
               date
               creator{
                email
            }
           }
           }
          `
    }

    const token = context.token;

    console.log(token.token)

    fetch("http://localhost:7000/graphql", {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token.token}`
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed.")
        }
        return res.json()

      }).then(resData => {
        // console.log(resData);
        fetchEvents(resData)
      })
      .catch(err => {
        console.log(err)
      })
  };

  const modalCancelHandler = () => {
    setCreating(false);
    setSelectedEvent(null);
  };


  const fetchEvents = () => {

    const requestBody = {
      query: `
          query {
            events {
              _id
               title
               price
               description
               date
               creator{
                email
                _id
              }
            }
          }
          `
    }

    const token = context.token;

    console.log(token.token)

    fetch("http://localhost:7000/graphql", {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed.")
        }
        return res.json()

      }).then(resData => {
        const events1 = resData.data.events;
        setEvents(events1)
      })
      .catch(err => {
        console.log(err)
      })

  }

  const showDetailHandler = (eventId) => {
    console.log(eventId, "bhjbkllllllll")
    console.log(eventId, events, "njkjbbkj")
    const selectedEvent = events.find(e => e._id === eventId);
    console.log(selectedEvent);
    return setSelectedEvent(selectedEvent)
  }

  const bookEventHandler = () => {

    if (!context.token) {
   
      setSelectedEvent(null)
      return;
    }
    const requestBody = {
      query: `
          mutation {
            bookEvent(eventId: "${selectedEvent._id}") {
              _id
              createdAt
              updatedAt
            }
          }
          `
    }

    const token = context.token;

    console.log(token.token)

    fetch("http://localhost:7000/graphql", {
      method: 'POST',
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token.token}`
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed.")
        }
        return res.json()

      }).then(resData => {
        console.log(resData);
        setSelectedEvent(null)

      })
      .catch(err => {
        console.log(err)
      })
  }

  return (
    <React.Fragment>
      {(creating || selectedEvent) && <Backdrop />}
      {creating && <Modal title="Add Event"
        canCancel canConfirm onCancel={modalCancelHandler}
        onConfirm={modalConfirmHandler}
        confirmText={context.token ? "Book" : "Confirm"}
      >
        <form>

          <div className='form-control'>
            <label htmlFor='title'>Title</label>
            <input type="text" id='title' name='title' onChange={onInputChange}></input>
          </div>

          <div className='form-control'>
            <label htmlFor='price'>Price</label>
            <input type="number" id='price' name='price' onChange={onInputChange}></input>
          </div>

          <div className='form-control'>
            <label htmlFor='date'>Date</label>
            <input type="datetime-local" id='date' name='date' onChange={onInputChange}></input>
          </div>

          <div className='form-control'>
            <label htmlFor='description'>Description</label>
            <textarea type="text" id='description' name='description' onChange={onInputChange}></textarea>
          </div>


        </form>
      </Modal>
      }

      {
        selectedEvent && (<Modal title={selectedEvent.title}
          canCancel canConfirm
          onCancel={modalCancelHandler}
          onConfirm={bookEventHandler}
          confirmText="Book"
        >
          <h1>{selectedEvent.title}</h1>
          <h2>{selectedEvent.price}</h2>
        </Modal>)
      }

      {
        context.token &&
        <div className='event-control'>
          <p>Share your own Events!</p>
          <button className='btn' onClick={createEventHandler}>Create Event</button>
        </div>
      }

      <EventList
        events={events}
        authUserId={context.userId}
        onViewDetail={showDetailHandler}
      />

    </React.Fragment>
  )
}

export default Events