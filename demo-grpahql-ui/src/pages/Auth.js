import React, { useContext, useState } from 'react'
import AuthContext from '../context/auth-context';

import './Auth.css'

const Auth = () => {

    const value = useContext(AuthContext);

    const [formData, setFormData] = useState({ email: '', password: '' });
    const [isLoging, setIsLoging] = useState(true);

    const onInputChange = (e) => {
        console.log(e.target.value)
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const switchModeHandler = () => {
        setIsLoging(!isLoging)
    }

    const submitHandler = (e) => {
        e.preventDefault()
        console.log("sdfa")
        if (formData.email.trim().length === 0 || formData.password.trim().length === 0) {
            return;
        }

        // console.log("sdfa", formData)

        let requestBody = {
            query: `
                query{
                    login(email: "${formData.email}", password:"${formData.password}"){
                        userId
                        token
                        tokenExpiration
                    }
                }
           `
        }


        if (!isLoging) {
            requestBody = {
                query: `
                mutation{
                    createUser(userInput:{
                     email: "${formData.email}",
                     password: "${formData.password}"
                   }){
                     email
                     password
                 }
                 }
                `
            }
        }


        fetch("http://localhost:7000/graphql", {
            method: 'POST',
            body: JSON.stringify(requestBody),
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(res => {
                if (res.status !== 200 && res.status !== 201) {
                    throw new Error("Failed.")
                }
                return res.json()

            }).then(resData => {
                if (resData.data.login.token) {
                    value.login(resData.data.login.token, resData.data.login.userId, resData.data.login.tokenExpiration)
                }
            })
            .catch(err => {
                console.log(err)
            })

    };

    return (
        <div className="auth-form">
            <form onSubmit={submitHandler}>
                <div className='form-control'>
                    <label htmlFor='email'>E-Mail</label>
                    <input type="email" name="email" onChange={onInputChange} />
                </div>
                <div className='form-control'>
                    <label htmlFor='password'>Password</label>
                    <input type="password" name="password" onChange={onInputChange} />
                </div>

                <div className='form-actions'>
                    <button type='submit'>Login</button>
                    <button type='button' onClick={switchModeHandler}> Switch to {isLoging ? 'Signup' : 'Login'}</button>
                </div>
            </form>
        </div>
    )
}

export default Auth